const BaseModule = require('./BaseModule.js');

class MemeModule extends BaseModule {
  constructor() {
    super();
    super.callsign = "memeify";
    super.description = "For all your memeing needs\n" +
      "  @Calypso memeify enscribe \"Enscribe me\"\n  @Calypso memeify spongebob \"mock me\"";
    super.addAction("inscribe", this.enscribe, ["enscribe", "e", "text"]);
    super.addAction("spongebob", this.spongeBob, ["mock", "s", "mocking spongebob"]);
    super.match = super.defaultMatch();

  }

  /**
   * Enscribe the message using the regional indicator emoticon.
   * 
   * @param {string} msg 
   * @param {string} args 
   */
  enscribe(msg, args = {}) {
    let returnStr = "", msgContent = args.quoted.toLowerCase();
    for (var i = 0, len = args.quoted.length; i < len; i++) {
      if (msgContent[i] === " ") {
        returnStr += " ";
      } else if (/[a-z]/i.test(msgContent[i])) {
        returnStr += `:regional_indicator_${msgContent[i]}:`
      } else if (/\d/i.test(msgContent[i])) {
        switch (msgContent[i]) {
          case "1":
            returnStr += ":one:";
            break;
          case "2":
            returnStr += ":two:";
            break;
          case "3":
            returnStr += ":three";
            break;
          case "4":
            returnStr += ":four:";
            break;
          case "5":
            returnStr += ":five:";
            break;
          case "6":
            returnStr += ":six:";
            break;
          case "7":
            returnStr += ":seven:";
            break;
          case "8":
            returnStr += ":eight:";
            break;
          case "9":
            returnStr += ":nine:";
            break;
          case "0":
            returnStr += ":zero:";
            break;
          default:
            break;
        }
      }
    }
    if (returnStr)
      msg.channel.send(returnStr).catch(err => {
        console.log(err);
      });;
  }

  /**
   * Enscribe the message using the spongebob meme.
   * 
   * @param {string} msg
   * @param {string} args
   */
  spongeBob(msg, args = {}) {
    let returnStr = "", msgContent = args.quoted;
    for (let i = 0, len = args.quoted.length; i < len; i++) {
      let seed = Math.floor(Math.random() * 2 + 1);
      if (seed == 1) {
        returnStr += msgContent[i].toLowerCase();
      } else {
        returnStr += msgContent[i].toUpperCase();
      }
    }
    msg.channel.send(returnStr, { file: __dirname + "/img/spongebob.jpg" }).catch(err => {
      // if (err.message ==="Missing Permissions"){
      msg.channel.send(
        {
          embed: {
            description: returnStr,
            image: {
              url: "https://uproxx.files.wordpress.com/2017/05/mocking-spongebob.jpg?quality=100&w=650"
            }
          }
        });
      // }
    });
  }


}

module.exports = MemeModule;