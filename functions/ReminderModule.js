const BaseModule = require('./BaseModule.js');

/**
 * This module will be used to set reminders at certian times and 
 * remind the user that posted them some time in the future.
 */
class RemindMe extends BaseModule {

  constructor() {
    super();
    this.callsign = "remindme";
    this.description = "This module is used to remind you of something";
    this.match = new RegExp(`${this.callsign} (\\d+|\\d+\.\\d) (day|hour|minute)(?:s)? (.+)?`, "i");
  }

  /**
   * Override the BaseModules execute.
   * 
   * @param {*} main_class 
   * @param {*} msg 
   * @param {*} args 
   */
  async execute(main_class, msg, args = {}) {
    let com = this.match.exec(msg);
    // console.log(com);
    let response = `Hey ${msg.author.toString()} you asked me to remind you about "${com[3]}".`, time;
    switch (com[2]) {
      case "day":
        time = com[1] * 86400000;
        break;
      case "hour":
        time = com[1] * 3600000;
        break;
      case "minute":
      default:
        time = com[1] * 60000;
        break;
    }
    setTimeout(function () {
      msg.channel.send(response);
    }, time);
    msg.channel.send("Ok.");
  }
}

module.exports = RemindMe;