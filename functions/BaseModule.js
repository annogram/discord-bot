/**
 * The base modue that any function this bot uses can be added to. For the bot to accept
 * a new function it must extend this class.
 */
class BaseModule {
  constructor() {
    this.callsign = "BASE";
    this.description = "This is the Base text, this should be replaced by a description of the action";
    this.actions = {
      "": msg => {
        msg.channel.send(this.description);
      },
      "help": (msg) => {
        msg.channel.send(this.description);
      }
    }
    this.match = this.defaultMatch();
  }

  /**
  * This will be called when the commands a child command is executed.
  * 
  * @param {string} command 
  */
  async execute(main_class, msg, args = {}) {
    var com = this.match.exec(msg);
    args["quoted"] = com[3];
    this.actions[com[1]].call(main_class, msg, args);
  }

  /**
   * Hook method to add a new command to the set of commands that the module can execute
   * 
   * @param {string} command 
   * @param {function} func 
   * @param {string[]} commandSynonyms 
   */
  addAction(command, func, commandSynonyms = [""]) {
    this.actions[command] = func;
    if (commandSynonyms.length > 1) {
      commandSynonyms.forEach((e, i) => {
        this.actions[e] = func;
      }, this);
    }
  }

  /**
   * Helper method to get all the keys in the action.
   * @returns {string[]} keys
   */
  collateKeys() {
    var keys = [];
    for (var key in this.actions) {
      if (key !== '')
        keys.push(key);
    }
    return keys;
  }

  defaultMatch() {
    return new RegExp(`${this.callsign}\\s(${this.collateKeys().join('|')})(\\s"(.*)")?`, 'i');
  }
}

module.exports = BaseModule;