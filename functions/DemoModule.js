const baseModule = require('./BaseModule.js');
const fs = require('fs');

class DemoModule extends baseModule {
    constructor() {
        super();
        super.callsign = "demo";
        super.match = new RegExp(`${this.callsign}\\b`, 'i');
        super.description = "This is a dynamically loaded in set of instructions."
        this.actions["test"] = (msg, args = {}) => {
            msg.channel.send("shut up " + msg.author.toString()).catch(err => {
                console.log(err);
            });;
            msg.channel.send("My name is " + args.client.user.toString()).catch(err => {
                console.log(err);
            });
        }
        this.actions["save"] = this.SaveText;
        super.match = new RegExp(`${this.callsign}\\s(${this.collateKeys().join('|')})`, "i");
    }

    SaveText(msg, args = {}) {
        console.log(msg.content);
        console.log(args.command);
    }
}

module.exports = DemoModule;