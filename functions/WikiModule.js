const BaseModule = require('./BaseModule');
const Wiki = require('wikijs').default;
const WikiParse = require('infobox-parser');

class WikiModule extends BaseModule {
  constructor() {
    super();
    super.callsign = 'wiki';
    super.description = '```Search wikipedia for information'
      + '\n@Calypso wiki search "Batman"\n```';
    super.addAction('search', this.search, ['s', 'Search', 'find']);
    // Do this last
    this.match = this.defaultMatch();
  }

  search(msg, args = {}) {
    let search = args.quoted;
    // let contentList = [];
    Wiki().page(search)
      .then(page => page.summary())
      .then(content => {
        while(content.length > 2000){
          msg.channel.send(content.substr(0, 1999));
          content = content.substr(2000, content.length);
        }
        msg.channel.send(content);
      }, () => {
        let refine = 'Sorry couldn\'t find anything about that.';
        msg.channel.send(refine);
      }).catch(console.error);
  }
}

module.exports = WikiModule;