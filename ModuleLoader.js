const fs = require('fs');
const path = require('path');
const baseModule = require('./functions/BaseModule.js');

class ModuleLoader {
  /**
   * Module loader will take in the path of directory containing the modules in its
   * constructor and load in those modules.
   *
   * @constructor
   * @param {string} dir
   */
  constructor(dir) {
    this.modules = {};

    fs.readdir(dir, (_, files) => {
      console.log("expanding consciousness...");
      files.forEach((e, i) => {
        if (fs.statSync(path.join(dir, e)).isDirectory())
          return;
        if (e.match(/\.js$/i) && !e.match("BaseModule.js")) {
          // check if the moduel extends base module otherwise reject it.
          console.log(`...found ${e}`);
          let dynModule = require(path.join(dir, e));

          let operation = new dynModule();
          if (operation instanceof baseModule) {
            this.modules[operation.callsign] = operation;
          } else {
            console.info("DISGUSTING..This isn't mine");
          }
        }
      });
    });
  }
}

module.exports = ModuleLoader;