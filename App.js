const Discord = require('discord.js');
const env = require('config.json')('./bot.json');
const ModuleLoader = require('./ModuleLoader.js');

const client = new Discord.Client();
const moduleLoader = new ModuleLoader(__dirname + '/functions');
const modules = moduleLoader.modules;

client.on('ready', () => {
  console.log("I have awoken.");
});

client.on('message', message => {
  // Do not process messages that this bot posts
  if (message.author.toString() !== client.user.toString()) {
    if (message.author.bot)
      return;

    let cmdRegx = new RegExp(`.*(<@${env.clientID}>)\\s+(.*)`, 'i');
    let matchRgx = cmdRegx.exec(message.content), matchStr;
    var responded;
    if (matchRgx) {
      matchStr = matchRgx[2];
      responded = false;
    }

    for (var key in modules) {
      if (modules[key].match.test(matchStr)) {
        try {
          console.log("Acknowledged.");
          modules[key].execute(this, message, { "client": client }).catch(err => {
            console.log(err);
          });
          responded = true;
        } catch (TypeError) {
          console.log("Negative Acknowledgement");
          break;
        }
        if (!responded) {
          message.channel.send(`Sorry ${message.author.toString()}, I'm afraid i can't do that`,
            { tts: true }).catch(err => {
              console.log(err);
            });
        }
        break;
      }
    }
  }
});

client.login(env.clientToken);
