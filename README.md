# Discord bot

This bot as made using Node.js as a project to learn about the language, it uses the offical discord Node api to interface with discord.

## Prerequisites

* Must have a version of npm 4.0+
* Must have Node.js installed

## Installation

* Download the repository
	 * Either through some source controll or directly
* From the root project directory run `npm install`

## Running

Launch the bot by running the main file _App.js_ from node:

`node App.js`

## Add to server 

https://discordapp.com/api/oauth2/authorize?client_id=348437835382259723&scope=bot&permissions=1